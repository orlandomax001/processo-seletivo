/*
 * ProdutoDAOTest.java
 * Copyright (c) Hepta.
 *
 * Este software é confidencial e propriedade da Hepta.
 * Não é permitida sua distribuição ou divulgação do seu conteúdo sem expressa autorização da Hepta.
 * Este arquivo contém informações proprietárias.
 */
package com.hepta.mercado.test;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import com.hepta.mercado.entity.Fabricante;
import com.hepta.mercado.entity.Produto;
import com.hepta.mercado.persistence.FabricanteDAO;
import com.hepta.mercado.persistence.ProdutoDAO;

/**
 * Implementação dos testes referentes a classe de négocio {@link Produto}.
 * 
 * @author jose Orlando
 */
public class ProdutoDAOTest {
	ProdutoDAO produtoDao = new ProdutoDAO();
	
	FabricanteDAO fabricanteDAO = new FabricanteDAO();

	private final String PRODUTO_NAO_ENCONTADO = "Produto não encontrado na Base de Dados.";
	private final String PRODUTO_EXCLUIDO_SUCESSO = "O produto foi excluído com sucesso!";
	private final String PRODUTO_SALVO_SUCESSO = "O produto foi Salvo com Sucesso!";

	@Test
	public void salvar() {
		try {
			Produto produto = new Produto();
			produto.setNome("Arroz");
			produto.setUnidade("Kg");
			produto.setVolume(1.0);
			produto.setEstoque(50);
			
			Fabricante fabricante = fabricanteDAO.find(1);
			if(fabricante != null) {
				produto.setFabricante(fabricante);
			}
			produtoDao.save(produto);
			System.out.println(PRODUTO_SALVO_SUCESSO);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	@Ignore
	public void findById() {
		try {
			Produto produto = produtoDao.find(8);
			if (produto != null) {
				System.out.println(produto.getNome());
				System.out.println(produto.getUnidade());
				System.out.println(produto.getVolume());
			} else {
				System.err.println(PRODUTO_NAO_ENCONTADO);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	@Ignore
	public void update() {
		try {
			Produto produto2 = produtoDao.find(9);

			if (produto2 != null) {
				produto2.setNome("Açucar");
				produto2.setUnidade("Kg");
				produto2.setVolume(1.0);
				produto2.setEstoque(30);
			} else {
				System.err.println(PRODUTO_NAO_ENCONTADO);
			}

			produtoDao.update(produto2);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	@Ignore
	public void getAll() {
		try {
			List<Produto> produtos = produtoDao.getAll();
			if (!produtos.isEmpty()) {
				produtos.forEach(p -> System.out.println(p.getId() + "- " + "Produto " + p.getNome()));
			} else {
				System.out.println(PRODUTO_NAO_ENCONTADO);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	@Ignore
	public void delete() {
		try {
			Produto produto = produtoDao.find(10);
			if (produto != null) {
				produtoDao.delete(produto.getId());
				System.out.println(PRODUTO_EXCLUIDO_SUCESSO);
			} else {
				System.err.println(PRODUTO_NAO_ENCONTADO);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
