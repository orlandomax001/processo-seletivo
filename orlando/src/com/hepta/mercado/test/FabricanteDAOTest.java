/*
 * ProdutoDAOTest.java
 * Copyright (c) Hepta.
 *
 * Este software é confidencial e propriedade da Hepta.
 * Não é permitida sua distribuição ou divulgação do seu conteúdo sem expressa autorização da Hepta.
 * Este arquivo contém informações proprietárias.
 */
package com.hepta.mercado.test;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import com.hepta.mercado.entity.Fabricante;
import com.hepta.mercado.persistence.FabricanteDAO;

/**
 * Implementação dos testes referentes a classe de négocio {@link Fabricante}.
 * 
 * @author jose Orlando
 */
public class FabricanteDAOTest {

	private final String MESSAGE_SUCESS = "Fabricante salvo na Base de Dados com sucesso!";
	private final String MESSAGE_UPDATE_SUCESS = "Fabricante atualizado na Base de dados com sucesso!";
	private final String MESSAGE_FABRICANTE_ENCONTRADO = "O Fabricante encontrado na Base de Dados é: ";
	private final String MESSAGE_NOT_FOUND = "O Fabricante informado não foi encontrado na Base de dados";

	FabricanteDAO fabricantedao = new FabricanteDAO();

	@Test
	public void salvar() {
		Fabricante fabricante = new Fabricante();
		fabricante.setNome("Tio Jorge");
		try {
			fabricantedao.save(fabricante);
			System.out.println(MESSAGE_SUCESS);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	@Ignore
	public void update() {
		Fabricante fabricante = new Fabricante();
		fabricante.setNome("Assolan");
		try {
			fabricantedao.update(fabricante);
			System.out.println(MESSAGE_UPDATE_SUCESS);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	@Ignore
	public void findById() {
		try {
			Fabricante fabricante = fabricantedao.find(1);
			if (fabricante != null) {
				System.out.println(MESSAGE_FABRICANTE_ENCONTRADO + fabricante.getNome());
			} else {
				System.out.println(MESSAGE_NOT_FOUND);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	@Ignore
	public void getAllFabricantes() {
		try {
			List<Fabricante> fabricantes = fabricantedao.getAll();
			if (!fabricantes.isEmpty()) {
				fabricantes.forEach(p -> System.out.println(p.getId() + "- " + "Fabricante : " + p.getNome()));
			} else {
				System.out.println(MESSAGE_NOT_FOUND);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	@Ignore
	public void delete() {
		try {
			Fabricante fabricante = fabricantedao.find(1);
			if (fabricante != null) {
				fabricantedao.delete(fabricante.getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
