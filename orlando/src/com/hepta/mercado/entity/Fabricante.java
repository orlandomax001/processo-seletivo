/*
 * Fabricante.java
 * Copyright (c) Hepta.
 *
 * Este software é confidencial e propriedade da Hepta.
 * Não é permitida sua distribuição ou divulgação do seu conteúdo sem expressa autorização da Hepta.
 * Este arquivo contém informações proprietárias.
 */
package com.hepta.mercado.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Classe de representação de 'Fabricante'.
 *
 * @author jose Orlando.
 */
@Entity
public class Fabricante implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_FABRICANTE")
	private Integer id;

	@Column(name = "NOME")
	private String nome;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

}