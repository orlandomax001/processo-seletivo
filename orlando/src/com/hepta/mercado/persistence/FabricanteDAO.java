/*
 * ProdutoDAOTest.java
 * Copyright (c) Hepta.
 *
 * Este software é confidencial e propriedade da Hepta.
 * Não é permitida sua distribuição ou divulgação do seu conteúdo sem expressa autorização da Hepta.
 * Este arquivo contém informações proprietárias.
 */
package com.hepta.mercado.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.hepta.mercado.entity.Fabricante;

/**
 * Implementação dos testes referentes a classe de négocio {@link Fabricante}.
 * 
 * @author jose Orlando
 */
public class FabricanteDAO {

	/**
	 * Inclui um {@link Fabricante} na Base de dados.
	 * 
	 * @param produto
	 * @throws Exception
	 */
	public void save(Fabricante fabricante) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(fabricante);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
	}

	/**
	 * Atualiza o {@link Fabricante} na Base de dados.
	 * 
	 * @param fabricante
	 * @return
	 * @throws Exception
	 */
	public Fabricante update(Fabricante fabricante) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		Fabricante fabricanteAtualizado = null;
		try {
			em.getTransaction().begin();
			fabricanteAtualizado = em.merge(fabricante);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return fabricanteAtualizado;
	}

	/**
	 * Deleta um {@link Fabricante} na Base de dados.
	 * 
	 * @param id
	 * @throws Exception
	 */
	public void delete(Integer id) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		try {
			em.getTransaction().begin();
			Fabricante fabricante = em.find(Fabricante.class, id);
			em.remove(fabricante);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}

	}

	/**
	 * Retorna um {@link Fabricante} pelo Id cadastrado na Base de dados.
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public Fabricante find(Integer id) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		Fabricante fabricante= null;
		try {
			fabricante = em.find(Fabricante.class, id);
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return fabricante;
	}

	/**
	 * Retorna todos os {@link Fabricante} da Base de dados.
	 * 
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<Fabricante> getAll() throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		List<Fabricante> fabricantes = new ArrayList<>();
		try {
			Query query = em.createQuery("FROM Fabricante");
			fabricantes = query.getResultList();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return fabricantes;
	}
}
